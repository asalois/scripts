#!/bin/bash
tmp_dir=$(mktemp -d -t kernel-test-XXXXXXXXXX)
k=5.15.56
echo $tmp_dir
echo $k
sleep 2
cd $tmp_dir
wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$k.tar.xz
tar -xvf linux-$k.tar.xz
cd linux-$k
make defconfig
time make -j $(nproc)
cd ~/
rm -rf $tmp_dir

